import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { DestinoViaje } from '../destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino : DestinoViaje
  @HostBinding('attr.class') p = 'lead'

  constructor() {}

  ngOnInit(): void {
  }

}
