import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  destinos : DestinoViaje[] = []

  constructor() { }

  ngOnInit(): void {
  }

  guardar(nombre : string, estancia : string){
    if(nombre == '' || estancia == ''){
      this.destinos.push(new DestinoViaje('Sin Registro', 'Sin registro'))
    }else{
      this.destinos.push(new DestinoViaje(nombre, estancia))
    }
    console.log(this.destinos)
  }

}
