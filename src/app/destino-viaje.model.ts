export class DestinoViaje {
    nombre : string
    estancia : string

    constructor( nombre : string, estancia : string){
        this.nombre = nombre
        this.estancia = estancia
    }
}